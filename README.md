# Spring Security & JWT with Spring Boot 2.0 Rest API application

Development environment

* Java 1.8.21
* Spring Boot 2.0.5
* H2
* java-jwt 3.4.0
* Maven 3.5.4

#### Build & Run
using an embedded database H2.
```
mvn clean package
java -jar .\target\springboot-security-jwt-demo.jar
```

List user login

|email|password|admin|
|---|---|---|
|naruto@example.com|iWKw06pvj|true|
|asuma@example.com|sk10ZIaiq|false|
|sasuke@example.com|me02yFufL|false|
|sakura@example.com|FjqU39aia|false|
|kakashi@example.com|ruFOep18r|false|


# API
### login API
After successful authentication, you can get a token. This token is used with the API that requires authentication.
```
curl -i -X POST "http://localhost:8080/app/login" -d "email=naruto@example.com" -d "pass=iWKw06pvj"
```
For example, the response is as follows
```
HTTP/1.1 200
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwibmJmIjoxNTM5NjA1MjkyLCJleHAiOjE1Mzk2MDU4OTIsImlhdCI6MTUzOTYwNTI5Mn0.ghP6Wvay-3Uj8V97Ipnu1tyHktWafgWXZV18jir6DbpRij7ol4pGi752CRKhxm0Zo-mALBr3UdZM29cMixQvVw
X-Content-Type-Options: nosniff
X-XSS-Protection: 1; mode=block
Cache-Control: no-cache, no-store, max-age=0, must-revalidate
Pragma: no-cache
Expires: 0
X-Frame-Options: DENY
Content-Length: 0
Date: Mon, 15 Oct 2018 12:08:12 GMT
```

### No authentication required API
`curl -i "http://localhost:8080/app/hello"`

`curl -i "http://localhost:8080/app/hello/{message}"`

`curl -i -X POST "http://localhost:8080/app/hello" -d "message=world"`

### These APIs do not need roles
But the user needs to be authenticated.

`curl -i -H "Authorization: Bearer {TOKEN}" "http://localhost:8080/app/memo/1"`

`curl -i -H "Authorization: Bearer {TOKEN}" "http://localhost:8080/app/memo/list"`

### These APIs requiring authentication and USER role

`curl -i -H "Authorization: Bearer {TOKEN}" "http://localhost:8080/app/user"`

`curl -i -H "Authorization: Bearer {TOKEN}" "http://localhost:8080/app/user/echo/{message}"`

`curl -i -H "Authorization: Bearer {TOKEN}" -H "Content-Type:application/json" -X POST "http://localhost:8080/app/user/echo" -d "{\"message\": \"hello world\"}"`

### These APIs requiring authentication and ADMIN role
`curl -i -H "Authorization: Bearer {TOKEN}" "http://localhost:8080/app/admin"`

`curl -i -H "Authorization: Bearer {TOKEN}" "http://localhost:8080/app/admin/echo/{message}"`

`curl -i -H "Authorization: Bearer {TOKEN}" -H "Content-Type:application/json" -X POST "http://localhost:8080/app/admin/echo" -d "{\"message\": \"hello world\"}"`