package com.hendisantika.springbootsecurityjwtdemo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-jwt-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/10/18
 * Time: 12.38
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "memo")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Memo implements Serializable {

    private static final long serialVersionUID = 8836618159506901418L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false, length = 255)
    private String title;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "done", nullable = false)
    private Boolean done;

    @Column(name = "updated", nullable = false)
    private LocalDateTime updated;

    public static Memo of(String title, String description) {
        return Memo.of(null, title, description);
    }

    public static Memo of(Long id, String title, String description) {
        return Memo.builder()
                .id(id)
                .title(title)
                .description(description)
                .done(false)
                .updated(LocalDateTime.now())
                .build();
    }

    public void merge(Memo memo) {
        if (memo.title != null && memo.title.length() > 0) {
            title = memo.title;
        }
        if (memo.description != null && memo.description.length() > 0) {
            description = memo.description;
        }
        if (memo.done != null) {
            done = memo.done;
        }
    }

    @PrePersist
    private void prePersist() {
        done = false;
        updated = LocalDateTime.now();
    }

    @PreUpdate
    private void preUpdate() {
        updated = LocalDateTime.now();
    }

}
