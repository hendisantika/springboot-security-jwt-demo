package com.hendisantika.springbootsecurityjwtdemo.repository;

import com.hendisantika.springbootsecurityjwtdemo.entity.Memo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-jwt-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/10/18
 * Time: 12.42
 * To change this template use File | Settings | File Templates.
 */
public interface MemoRepository extends JpaRepository<Memo, Long> {
}