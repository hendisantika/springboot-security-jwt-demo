package com.hendisantika.springbootsecurityjwtdemo.service;

import com.hendisantika.springbootsecurityjwtdemo.entity.Memo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-jwt-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/10/18
 * Time: 12.44
 * To change this template use File | Settings | File Templates.
 */
public interface MemoService {
    Optional<Memo> findById(Long id);

    Page<Memo> findAll(Pageable page);

    void store(Memo memo);

    void updateById(Long id, Memo memo);

    void removeById(Long id);
}
