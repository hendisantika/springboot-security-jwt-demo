package com.hendisantika.springbootsecurityjwtdemo.service;

import com.hendisantika.springbootsecurityjwtdemo.entity.User;
import com.hendisantika.springbootsecurityjwtdemo.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-jwt-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/10/18
 * Time: 12.45
 * To change this template use File | Settings | File Templates.
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> findByName(String name) {
        Objects.requireNonNull(name, "name must be not null");
        return userRepository.findFirstByName(name);
    }

}