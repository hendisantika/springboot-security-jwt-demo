package com.hendisantika.springbootsecurityjwtdemo.service;

import com.hendisantika.springbootsecurityjwtdemo.entity.User;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-jwt-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/10/18
 * Time: 12.44
 * To change this template use File | Settings | File Templates.
 */
public interface UserService {
    Optional<User> findByName(String name);
}
